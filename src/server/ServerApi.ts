import axios, { AxiosInstance } from 'axios'
import { PlayerStatus } from '../player/Player'
import { Server, Status } from './models/Server'

class ServerApi {
  private instance: AxiosInstance
  private readonly serverServiceAddr: string

  public constructor() {
    this.serverServiceAddr = process.env.DEFAULT_SERVERS_SERVICE_ADDR || 'http://panel.minekloud.com/api/servers'

    this.instance = axios.create({ baseURL: this.serverServiceAddr })
  }

  public async getServers(): Promise<Server[]> {
    const servers = this.instance
      .get('')
      .then((res) => {
        return res.data
      })
      .catch((error) => {
        console.log(
          `Error: something happen wrong while fetch servers from ServersService at ${this.serverServiceAddr} : ${error}`
        )
        return []
      })
    return servers as Promise<Server[]>
  }

  public async startServer(serverId: number): Promise<Server> {
    const servers = this.instance
      .patch(serverId.toString(), {status: Status.ON})
      .then((res) => {
        return res.data
      })
      .catch((error) => {
        console.log(
          `Error: something happen wrong while fetch servers from ServersService at ${this.serverServiceAddr} : ${error}`
        )
        return []
      })
    return servers as Promise<Server>
  }

  public async postPlayers(
    serverId: number,
    usernames: string[]
  ) {
    this.instance.post(
      serverId + '/players',
      usernames.map((username) => {
        return {
          username,
          status: PlayerStatus.connected,
        }
      })
    ).then(res => console.log(`Post players connected succes`))
    .catch(err => console.log(`Post players connected error : ${err}`))
  }

  public async deletePlayer(
    serverId: number,
    username: string
  ) {
    this.instance.delete(serverId + '/players/' + username)
      .then(res => console.log(`Delete player success`))
      .catch(err => console.log(`Error delete player : ${err}`))
  }
}

export default new ServerApi()
