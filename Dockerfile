FROM node:14-alpine

WORKDIR /app

COPY package* .
RUN npm ci --prod
COPY dist .
COPY config.json .

USER node

EXPOSE 25565

ENTRYPOINT ["node", "index.js"]